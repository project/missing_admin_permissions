---SUMMARY---

This module grants the missing permissions for the Administrator
role in hook_cron.

For a full description visit project page:
https://www.drupal.org/project/missing_admin_permissions

Bug reports, feature suggestions and latest developments:
http://drupal.org/project/issues/missing_admin_permissions

---INTRODUCTION---


None.


---REQUIREMENTS---


*None. (Other than a clean Drupal installation)


---INSTALLATION---

Install as usual. Place the entirety of this directory in the /modules 
folder of your Drupal installation. 

---CONFIGURATION---

None.


---CONTACT---

Current Maintainers:
*Balogh Zoltán (zlyware) - https://www.drupal.org/u/u/zoltán-balogh
